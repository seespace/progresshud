package tv.inair.progresshud;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import java.util.HashMap;
import java.util.Map;

import inair.app.DismissParam;
import inair.app.IAActivity;
import inair.app.IAContext;
import inair.app.IAFragment;
import inair.app.IANavigation;
import inair.app.PresentParam;
import inair.collection.CollectionChangedEventArgs;
import inair.collection.ObservableCollection;
import inair.event.Event;
import inair.exception.IllegalArgumentNullException;
import inair.input.SwipeEventArgs;
import inair.input.TouchEventArgs;
import inair.utils.Transform;
import inair.view.UITextView;
import inair.view.UIView;
import inair.view.UIViewDescriptor;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 */
public class UIProgressHUD {

  public static final String TAG = "UIProgressHUD";
  private static final long DEFAULT_DURATION = 3000;

  public static final float DEFAULT_IMAGE_SIZE = 80;

  private IANavigation container;

  final Map<String, Event.Listener<TouchEventArgs>> doubleTapListenerMap = new HashMap<>();
  final Map<String, Event.Listener<SwipeEventArgs>> swipeListenerMap = new HashMap<>();

  private HUDView layout;
  private ViewModel viewModel;
  private final ObservableCollection<Task> taskQ = new ObservableCollection<>();

  private static final UIProgressHUD instance = new UIProgressHUD();

  public static Drawable LOADING_DRAWABLE;
  public static Drawable TRANSPARENT_DRAWABLE;
  public static Drawable ERROR_DRAWABLE;
  public static Drawable SUCCESS_DRAWABLE;

  private UIProgressHUD() {
    taskQ.collectionDidChange.addListener(new Event.Listener<CollectionChangedEventArgs>() {
      @Override
      public void onTrigger(Object sender, CollectionChangedEventArgs args) {
        if (args.action == CollectionChangedEventArgs.CollectionChangedAction.Add) {
          final Task firstTask = (Task) args.newItems.get(0);
          if (args.newStartingIndex == 0) {
            if (!layout.isAppeared()) {
              firstTask.run();

              PresentParam param = PresentParam.create()
                .childStartingState(STARTING_STATE)
                .childState(CHILD_STATE)
                .parentState(PARENT_STATE)
                .keepTVScreenState()
                .disableDefaultDismissGesture()
                .duration(500);

              if (_canDismiss) {
                layout.currentHud = UIProgressHUD.this;
              }
              container.present(layout, param);

              if (layout.isAppeared()) {
                layout.spinner.start();
              }
            } else {
              layout.spinner.stop();
              firstTask.run();
              layout.spinner.start();
            }
          }

          for (int i = 0; i < args.newItems.size() - 1; i++) {
            Task before = (Task) args.newItems.get(i);
            final Task after = (Task) args.newItems.get(i + 1);
            pipeTask(before, after);
          }

          if (args.newStartingIndex > 0 && taskQ.size() > args.newStartingIndex) {
            Task lastTask = taskQ.get(args.newStartingIndex - 1);
            if (lastTask.promise().isPending()) {
              pipeTask(lastTask, firstTask);
            } else {
              layout.spinner.stop();
              firstTask.run();
              layout.spinner.start();
            }
          }
        }
      }
    });
  }

  public static UIProgressHUD with(IANavigation container) {
    if (container == null) {
      throw new IllegalArgumentNullException("container");
    }
    if (instance._resources == null) {
      instance._resources = ((IAContext) container).getAndroidContext().getResources();
      LOADING_DRAWABLE = instance._resources.getDrawable(R.drawable.spinner);
      TRANSPARENT_DRAWABLE = instance._resources.getDrawable(R.drawable.transparent);
      //ERROR_DRAWABLE = instance._resources.getDrawable(R.drawable.error);
      //SUCCESS_DRAWABLE = instance._resources.getDrawable(R.drawable.success);
      ERROR_DRAWABLE = TRANSPARENT_DRAWABLE;
      SUCCESS_DRAWABLE = TRANSPARENT_DRAWABLE;
    }
    if (instance.container == container && instance.isShowing()) {
      instance.reset();
      return instance;
    }

    instance.setup(container);
    return instance;
  }

  public UIProgressHUD basedOnFrame(UIView view) {
    if (hasContainer()) {

      viewModel.setContainer(view);
    }
    return this;
  }

  //region API
  public UIProgressHUD iconWidth(float width) {
    if (!taskQ.isEmpty()) {
      Task task = taskQ.get(taskQ.size() - 1);
      task.setIconWidth(width);
    }
    return this;
  }

  public UIProgressHUD iconHeight(float height) {
    if (!taskQ.isEmpty()) {
      Task task = taskQ.get(taskQ.size() - 1);
      task.setIconHeight(height);
    }
    return this;
  }

  public boolean isShowing() {
    if (taskQ.isEmpty()) {
      return false;
    }

    Task task = taskQ.get(taskQ.size() - 1);
    return task.promise().isPending();
  }

  public UIProgressHUD show() {
    return show("");
  }

  public UIProgressHUD show(int statusResId) {
    return show(LOADING_DRAWABLE, statusResId);
  }

  public UIProgressHUD show(String status) {
    return show(LOADING_DRAWABLE, status);
  }

  public UIProgressHUD show(Drawable drawable, int statusResId) {
    return show(drawable, _resources.getString(statusResId));
  }

  public UIProgressHUD show(int drawableResId, int statusResId) {
    return show(_resources.getDrawable(drawableResId), _resources.getString(statusResId));
  }

  public UIProgressHUD showError(String status) {
    return show(ERROR_DRAWABLE, status);
  }

  public UIProgressHUD showError(int statusResId) {
    return showError(_resources.getString(statusResId));
  }

  public UIProgressHUD showSuccess(String status) {
    return show(SUCCESS_DRAWABLE, status);
  }

  public UIProgressHUD showSuccess(int statusResId) {
    return showSuccess(_resources.getString(statusResId));
  }

  public UIProgressHUD show(int resId, String status) {
    return show(_resources.getDrawable(resId), status);
  }

  public UIProgressHUD show(Drawable drawable, String status) {
    return show(drawable, status, UITextView.TextAlignment.RIGHT);
  }

  public UIProgressHUD showMessage(int statusResId, UITextView.TextAlignment alignment) {
    return show(TRANSPARENT_DRAWABLE, _resources.getString(statusResId), alignment);
  }

  public UIProgressHUD showMessage(String status, UITextView.TextAlignment alignment) {
    return show(TRANSPARENT_DRAWABLE, status, alignment);
  }

  synchronized public UIProgressHUD show(Drawable drawable, String status, UITextView.TextAlignment alignment) {
    if (!container.isInitialized()) {
      return this;
    }

    Task task = new Task(viewModel);
    task.setDrawable(drawable);
    task.setMessage(status);
    task.setAlignment(alignment);
    taskQ.add(task);

    task.promise().done(new DoneCallback<Task>() {
      @Override
      public void onDone(Task result) {
        if (result.isThenDismiss()) {
          dismiss();
        }
      }
    });

    task.promise().fail(new FailCallback<String>() {
      @Override
      public void onFail(String reason) {
        Log.d(TAG, "Task cancelled, reason: " + reason);
      }
    });

    return this;
  }

  public static final UIViewDescriptor STARTING_STATE = UIViewDescriptor.create().alpha(0f).transform(Transform.fromIdentity().build()).sealed();
  public static final UIViewDescriptor CHILD_STATE = UIViewDescriptor.create().alpha(1f).transform(Transform.fromIdentity().build()).sealed();
  public static final UIViewDescriptor PARENT_STATE = UIViewDescriptor.create().alpha(0.25f).transform(Transform.fromIdentity()
    .build()).sealed();

  public boolean dismiss() {
    return dismiss(false);
  }

  public boolean dismiss(boolean force) {
    if (layout == null || !hasContainer()) {
      return true;
    }

    // clean up
    layout.setDataContext(null);
    boolean ret;
    if (force) {
      ret = layout.dismiss(FORCE_PARAM);
    } else {
      ret = layout.dismiss();
    }

    container = null;
    layout = null;
    viewModel = null;

    return ret;
  }

  private static final DismissParam FORCE_PARAM = DismissParam.create().duration(1);

  public UIProgressHUD in(long ms) {
    if (!taskQ.isEmpty()) {
      Task task = taskQ.get(taskQ.size() - 1);
      task.setDuration(ms);
    }
    return this;
  }

  public UIProgressHUD then() {
    if (!taskQ.isEmpty()) {
      Task task = taskQ.get(taskQ.size() - 1);
      if (task.getDuration() == Integer.MAX_VALUE) {
        task.setDuration(DEFAULT_DURATION);
      }
    }
    return this;
  }

  public UIProgressHUD canDismiss() {
    _canDismiss = true;
    return this;
  }
  //endregion

  //region Events
  public UIProgressHUD terminateAppOnSwipeLeft(final IAActivity activity) {
    terminateAppOnSwipe(activity, SwipeEventArgs.Direction.Left);
    return this;
  }

  public UIProgressHUD terminateAppOnSwipe(final IAActivity activity, final SwipeEventArgs.Direction direction) {
    if (hasContainer()) {
      swipeListenerMap.put(container.getClass().getName(), new Event.Listener<SwipeEventArgs>() {
        @Override
        public void onTrigger(Object sender, SwipeEventArgs args) {
          if (args.direction == direction) {
            onDismiss(new Event.Listener<Void>() {
              @Override
              public void onTrigger(Object sender, Void args) {
                activity.finish();
              }
            });
            dismiss(true);
          }
        }
      });
    }
    return this;
  }

  public UIProgressHUD onDoubleTap(Event.Listener<TouchEventArgs> listener) {
    if (hasContainer()) {
      doubleTapListenerMap.put(container.getClass().getName(), listener);
    }
    return this;
  }

  public UIProgressHUD onDismiss(Event.Listener<Void> listener) {
    layout.didDismiss.addListener(listener);
    return this;
  }

  void _hudDismissed(boolean dismissContainer) {
    if (dismissContainer && hasContainer()) {
      container.dismiss();
    }
    container = null;
  }

  private boolean hasContainer() {
    return container != null;
  }

  private void pipeTask(final Task before, final Task after) {
    before.setThenDismiss(false);
    before.promise().done(new DoneCallback<Task>() {
      @Override
      public void onDone(Task task) {
        layout.spinner.stop();
        after.run();
        layout.spinner.start();
      }
    });
  }
  //endregion

  //region Internal
  private void setup(final IANavigation container) {
    instance.dismiss(true);

    // setup for new hud
    instance.container = container;

    instance.layout = new HUDView();
    instance.viewModel = new ViewModel((IAContext) instance.container);
    instance.viewModel.setContainer(container.getRootView());
    instance.layout.setDataContext(instance.viewModel);

    instance.reset();
    _canDismiss = false;
    instance.layout.didPresent.addListener(new Event.Listener<Void>() {
      @Override
      public void onTrigger(Object sender, Void args) {
        if (hasContainer()) {
          Event.Listener<TouchEventArgs> doubleTapHandler = doubleTapListenerMap.get(container.getClass().getName());
          if (doubleTapHandler != null) {
            instance.layout.addViewEventListener(UIView.DoubleTapEvent, doubleTapHandler);
          }

          Event.Listener<SwipeEventArgs> swipeHandler = swipeListenerMap.get(container.getClass().getName());
          if (swipeHandler != null) {
            instance.layout.addViewEventListener(UIView.SwipeEvent, swipeHandler);
          }

          if (container instanceof IAFragment) {
            ((IAFragment) container).didDismiss.addListener(new Event.Listener<Void>() {
              @Override
              public void onTrigger(Object sender, Void args) {
                doubleTapListenerMap.remove(sender.getClass().getName());
                swipeListenerMap.remove(sender.getClass().getName());
              }
            });
          }
        }
      }
    });
  }

  private UIProgressHUD reset() {
    _canDismiss = true;

    if (viewModel != null) {
      viewModel.setIconWidth(UIProgressHUD.DEFAULT_IMAGE_SIZE);
      viewModel.setIconHeight(UIProgressHUD.DEFAULT_IMAGE_SIZE);
    }

    for (Task task : instance.taskQ) {
      if (task.promise().isPending()) {
        task.cancel();
      }
    }

    instance.taskQ.clear();
    // rebind event again
    return this;
  }

  private boolean _canDismiss = true;
  private Resources _resources;
  //endregion
}
