package tv.inair.progresshud;

import android.os.Bundle;

import inair.app.IAFragment;
import inair.event.Event;
import inair.input.SwipeEventArgs;
import inair.view.UIImageView;
import inair.view.UIView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 */
@SuppressWarnings("unchecked")
public class HUDView extends IAFragment {

  @Override
  public void onInitialize(Bundle savedInstanceState) {
    setRootContentView(R.layout.progresshud);
    spinner = ((UIImageView) findUIViewById(R.id.spinner));

    if (currentHud != null) {
      addViewEventListener(UIView.PreviewSwipeEvent, onSwipeToDismiss);
    }
  }

  UIImageView spinner;
  boolean enableCallback;
  UIProgressHUD currentHud;

  final Event.Listener<SwipeEventArgs> onSwipeToDismiss = new Event.Listener<SwipeEventArgs>() {
    @Override
    public void onTrigger(Object sender, SwipeEventArgs args) {
      if (args.direction == SwipeEventArgs.Direction.Right) {
        enableCallback = true;
        currentHud.dismiss(true);
      }
    }
  };

  @Override
  public void onLayoutDidPresent(IAFragment parent) {
    spinner.start();
  }

  @Override
  public void onLayoutDidDismiss(IAFragment parent) {
    if (spinner != null) {
      spinner.stop();
    }
    if (currentHud != null) {
      currentHud._hudDismissed(enableCallback);
    }
  }
}
