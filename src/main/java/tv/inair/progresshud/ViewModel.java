package tv.inair.progresshud;

import android.graphics.drawable.Drawable;

import inair.app.IAContext;
import inair.view.UILayeredNavigationView;
import inair.view.UILayeredViewItem;
import inair.view.UITextView;
import inair.view.UIView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 */
public class ViewModel extends inair.data.ViewModel {

  public ViewModel(IAContext context) {
    super(context);
  }

  private String mMessage;
  private Drawable mIcon;
  private UITextView.TextAlignment mAlignment = UITextView.TextAlignment.RIGHT;

  public String getMessage() {
    return mMessage;
  }

  public void setMessage(String message) {
    mMessage = message;
    notifyPropertyChanged("message");
    notifyPropertyChanged("iconX");
    notifyPropertyChanged("messageX");
    notifyPropertyChanged("alignment");
  }

  public UITextView.TextAlignment getAlignment() {
    return mAlignment;
  }

  public void setAlignment(UITextView.TextAlignment alignment) {
    mAlignment = alignment;
    notifyPropertyChanged("alignment");
  }

  //region Icon
  public Drawable getIcon() {
    return mIcon;
  }

  public void setIcon(Drawable icon) {
    mIcon = icon;
    notifyPropertyChanged("icon");
    notifyPropertyChanged("messageX");
  }

  private float mIconWidth = UIProgressHUD.DEFAULT_IMAGE_SIZE, mIconHeight = UIProgressHUD.DEFAULT_IMAGE_SIZE;

  public float getIconWidth() {
    return mIconWidth;
  }

  public void setIconWidth(float iconWidth) {
    mIconWidth = iconWidth;
    notifyPropertyChanged("iconWidth");
    notifyPropertyChanged("iconX");
  }

  public float getIconHeight() {
    return mIconHeight;
  }

  public void setIconHeight(float iconHeight) {
    mIconHeight = iconHeight;
    notifyPropertyChanged("iconHeight");
  }
  //endregion

  //region Container
  public void setContainer(UIView container) {
    if (container instanceof UILayeredNavigationView || container instanceof UILayeredViewItem) {
      setContainerX(UILayeredNavigationView.DEFAULT_NAVIGATION_X() - 100f);
      setContainerY(UILayeredNavigationView.DEFAULT_NAVIGATION_Y());
      setContainerZ(UILayeredNavigationView.DEFAULT_FIRST_LAYER_Z());

      setContainerWidth(UILayeredViewItem.DEFAULT_LAYER_WIDTH());
      setContainerHeight(UILayeredViewItem.DEFAULT_LAYER_HEIGHT());
    } else {
      setContainerX(container.getPositionX());
      setContainerY(container.getPositionY());
      setContainerZ(container.getPositionZ());

      setContainerWidth(container.getWidth());
      setContainerHeight(container.getHeight());
    }
  }

  private float containerX;
  private float containerY;
  private float containerZ;
  private float containerWidth;
  private float containerHeight;

  public float getContainerX() {
    return containerX;
  }

  public void setContainerX(float containerX) {
    this.containerX = containerX;
    notifyPropertyChanged("containerX");
  }

  public float getContainerY() {
    return containerY;
  }

  public void setContainerY(float containerY) {
    this.containerY = containerY;
    notifyPropertyChanged("containerY");
  }

  public float getContainerZ() {
    return containerZ;
  }

  public void setContainerZ(float containerZ) {
    this.containerZ = containerZ;
    notifyPropertyChanged("containerZ");
  }

  public float getContainerWidth() {
    return containerWidth;
  }

  public void setContainerWidth(float containerWidth) {
    this.containerWidth = containerWidth;
    setHudX((containerWidth - getResources().getInteger(R.integer.hud_width)) / 2);
    notifyPropertyChanged("containerWidth");
  }

  public float getContainerHeight() {
    return containerHeight;
  }

  public void setContainerHeight(float containerHeight) {
    this.containerHeight = containerHeight;
    setHudY((containerHeight - getResources().getInteger(R.integer.hud_height)) / 2);
    notifyPropertyChanged("containerHeight");
  }
  //endregion

  //region HUD
  private float hudX;
  private float hudY;

  public float getHudY() {
    return hudY;
  }

  public void setHudY(float hudY) {
    this.hudY = hudY;
    notifyPropertyChanged("hudY");
  }

  public float getHudX() {
    return hudX;
  }

  public void setHudX(float hudX) {
    this.hudX = hudX;
    notifyPropertyChanged("hudX");
  }

  //endregion

  public float getMessageX() {
    return mIcon != UIProgressHUD.TRANSPARENT_DRAWABLE ? (mIconWidth - UIProgressHUD.DEFAULT_IMAGE_SIZE - 125f) : 0f;
  }

  public float getIconX() {
    return (getResources().getInteger(R.integer.hud_width) - mIconWidth) / 2;
  }
}
